import uuid
import random
from django.db import models, transaction
from django.utils.translation import ugettext_lazy as _
from django.utils.functional import cached_property


class Planet(models.Model):
    """Планеты"""
    name = models.CharField(_('Planet:name'), max_length=500)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Planet')
        verbose_name_plural = _('Planets')


class Sith(models.Model):
    """Ситхи"""
    SHADOW_HANDS_COUNT = 3

    name = models.CharField(_('Sith:name'), max_length=500)
    planet = models.ForeignKey(Planet, on_delete=models.PROTECT)
    shadow_hands = models.PositiveIntegerField(_('Shadow hands count'), default=SHADOW_HANDS_COUNT)

    def use_shadow_hand(self, recruit):
        if self.shadow_hands > 0:
            self.shadow_hands -= 1
            recruit.teacher = self
            with transaction.atomic():
                self.save()
                recruit.save()
            return True
        else:
            return False

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Sith')
        verbose_name_plural = _('Siths')


class RecruitManager(models.Manager):
    def with_exams_finished(self, planet_id=None):
        result = super(RecruitManager, self).filter(teacher=None) \
            .annotate(num_answers=models.Count('answers')) \
            .filter(num_answers__gte=Recruit.QUESTIONS_LIMIT)
        if planet_id:
            result = result.filter(planet_id=planet_id)
        return result

    def get_sith_students(self, sith_id, with_excluded=False, with_dead=False):
        result = super(RecruitManager, self).filter(teacher=sith_id)
        if not with_excluded:
            result = result.exclude(extermination_status=Recruit.EXTERMINATION_NEED_TO)
        if not with_dead:
            result = result.exclude(extermination_status=Recruit.EXTERMINATION_SUCCESFULL)
        return result

    @staticmethod
    def get_random_question(recruit):
        if recruit.questions_are_answered:
            result = {
                'all_questions_are_answered': True
            }
        else:
            question = Question.get_random(excluded=models.Subquery(recruit.answers.values('question_id')))
            result = {
                'all_questions_are_answered': question is None
            }
            if question:
                result['question'] = question
                result['answers'] = question.answers.all()
        return result


class Recruit(models.Model):
    """Рекруты"""
    QUESTIONS_LIMIT = 3

    EXTERMINATION_DONT_NEED_TO = 0
    EXTERMINATION_NEED_TO = 10
    EXTERMINATION_SUCCESFULL = 20
    EXTERMINATION_STATUSES = (
        (EXTERMINATION_DONT_NEED_TO, _('Do not need to')),
        (EXTERMINATION_NEED_TO, _('Need to')),
        (EXTERMINATION_SUCCESFULL, _('Finished')),
    )
    objects = RecruitManager()

    name = models.CharField(_('Recruit:name'), max_length=500)
    age = models.PositiveIntegerField(_('Recruit:age'))
    email = models.CharField(_('Recruit:email'), max_length=500)
    planet = models.ForeignKey(Planet, on_delete=models.PROTECT)
    teacher = models.ForeignKey(Sith, verbose_name=_('Recruit:teacher'),
            on_delete=models.SET_NULL, null=True, default=None, blank=True)
    time_registered = models.DateTimeField(_('Recruit:Time registered'), auto_now_add=True)
    extermination_status = models.PositiveSmallIntegerField(_('Recruit:Extermination status'),
            choices=EXTERMINATION_STATUSES, default=EXTERMINATION_DONT_NEED_TO)

    def __str__(self):
        return '{0} {1} age: {2}. {3}'.format(self.name, self.email, self.age, 'student' if self.teacher else 'recruit')

    @cached_property
    def answered_questions(self):
        """Get count of answered questions as integer."""
        return self.answers.all().count()

    @cached_property
    def questions_are_answered(self):
        return self.answers.count() >= Recruit.QUESTIONS_LIMIT

    class Meta:
        verbose_name = _('Recruit')
        verbose_name_plural = _('Recruits')


class Question(models.Model):
    """Вопросы рекруту"""
    orden_uuid = models.CharField(_('Question:ordern_uid'), max_length=37, blank=True)
    text = models.TextField(_('Question:text'))

    def __str__(self):
        return self.text

    @staticmethod
    def generate_orden_uuid():
        return uuid.uuid4()

    @staticmethod
    def get_random(excluded=None):
        questions = Question.objects.all()
        if excluded:
            questions = questions.exclude(id__in=excluded)
        return random.choice(questions) \
            if len(questions) > 0 \
            else None

    def save(self, *args, **kwargs):
        if not self.orden_uuid:
            self.orden_uuid = self.generate_orden_uuid()
        super(Question, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _('Question')
        verbose_name_plural = _('Questions')


class Answer(models.Model):
    """Варианты ответов на вопросы"""
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='answers')
    position = models.IntegerField(_('Answer:position'), default=0, blank=True)
    text = models.TextField(_('Answer:text'))

    def __str__(self):
        return '{0}: {1}'.format(self.question_id, self.text)

    class Meta:
        verbose_name = _('Question')
        verbose_name_plural = _('Questions')


class RecruitAnswer(models.Model):
    """Ответы рекрута на вопросы"""
    recruit = models.ForeignKey(Recruit, on_delete=models.CASCADE, related_name='answers')
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='+')
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE, related_name='+')
    time_answered = models.DateTimeField(_('RecruitAnswer:Time answered'), auto_now_add=True)

    def __str__(self):
        return '{0}: ({1}) {2}'.format(self.recruit_id, self.question_id, self.answer_id)

    class Meta:
        verbose_name = _('Recruit Answer')
        verbose_name_plural = _('Recruit Answers')
