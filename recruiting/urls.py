from django.urls import path
from django.views.generic import TemplateView
from .views import recruit, sith
from .apps import RecruitConfig

recruit_urls = [
    path('recruit-register/', recruit.RecruitRegister.as_view(), name='recruit-register'),
    path('recruit-questions/<int:recruit_id>/', recruit.RecruitQuestions.as_view(), name='recruit-questions'),
    path('recruit-personal/<int:recruit_id>/', recruit.RecruitPersonal.as_view(), name='recruit-personal'),
    path('recruits/', recruit.RecruitList.as_view(), name='recruits'),
]

sith_urls = [
    path('sith-entry/', sith.SithEntry.as_view(), name='sith-entry'),
    path('sith-personal/<int:sith_id>/', sith.SithPersonal.as_view(), name='sith-personal'),
    path('sith-recruit/<int:sith_id>/<int:recruit_id>/', sith.SithRecruit.as_view(), name='sith-recruit'),
    path('sith-student/<int:sith_id>/<int:recruit_id>/', sith.SithStudent.as_view(), name='sith-student'),
    path('siths-with-many-sh/', sith.SithsWithMoreThanOneSH.as_view(), name='siths-with-many-sh')
]

app_name = RecruitConfig.name
urlpatterns = [
    path('', TemplateView.as_view(template_name='recruting/index-page.html'), name='index'),
] + recruit_urls + sith_urls
