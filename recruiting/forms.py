from django.forms import Form, ChoiceField, IntegerField, RadioSelect, HiddenInput
from django.core.exceptions import ValidationError
from django.db.models import QuerySet
from .models import Question


class AnswersForm(Form):
    question_id = IntegerField(widget = HiddenInput())
    answers = IntegerField(widget = HiddenInput())

    def __init__(self, *args, **kwargs):
        super(AnswersForm, self).__init__(*args, **kwargs)

        if 'data' in kwargs and 'answers' in kwargs['data']:
            answers = kwargs['data']['answers']
            if isinstance(answers, QuerySet):
                choices = [(a.id, a.text) for a in answers]
            else:
                choices = [(a, None) for a in answers]
            self.fields['answers'] = ChoiceField(
                choices=choices,
                widget=RadioSelect
            )

    def clean(self):
        cleaned_data = super().clean()
        if 'question_id' in cleaned_data and 'answers':
            q_id, a_id = int(cleaned_data['question_id']), int(cleaned_data['answers'])
            answer_ids = Question.objects.get(id=q_id).answers.all().values_list('id', flat=True)
            if not a_id in set(answer_ids):
                raise ValidationError('Answer with id {0} is not for a question {1}'.format(a_id, q_id))
        return cleaned_data
