from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages

from .base_views import MyListView, MyDetailView, BaseView
from ..models import Recruit, RecruitAnswer
from ..forms import AnswersForm


class RecruitRegister(CreateView):
    """Register as an recruit"""
    template_name = 'recruting/recruit/register.html'
    model = Recruit
    fields = ('name', 'age', 'email', 'planet',)

    def get_success_url(self):
        return reverse_lazy('recruiting:recruit-personal', kwargs={'recruit_id': self.object.id})


class RecruitList(MyListView):
    queryset = Recruit.objects.order_by('name').select_related('planet')
    context_object_name = 'recruits'
    template_name = 'recruting/recruit/list.html'


class RecruitPersonal(MyDetailView):
    model = Recruit
    context_object_name = 'recruit'
    pk_url_kwarg = 'recruit_id'
    template_name = 'recruting/recruit/personal.html'


class RecruitQuestions(BaseView):
    template_name = 'recruting/recruit/question.html'

    def get(self, request, *args, **kwargs):
        context = self.get_extra_context(kwargs)
        if 'recruit' not in context:
            return HttpResponseRedirect(
                reverse_lazy('recruiting:recruit-register')
            )
        result = Recruit.objects.get_random_question(context['recruit'])
        if result['all_questions_are_answered']:
            messages.success(request, _('You answered for all the questions'))
            return HttpResponseRedirect(
                reverse_lazy('recruiting:recruit-personal', kwargs={'recruit_id': context['recruit'].id})
            )
        context['question'] = result['question']
        context['answers_form'] = AnswersForm(data={
            'question_id': result['question'].id,
            'answers': result['question'].answers.all()
        })
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        context = self.get_extra_context(kwargs)
        answers_form = AnswersForm(request.POST)
        if answers_form.is_valid():
            data = answers_form.clean()
            RecruitAnswer.objects.create(
                recruit=context['recruit'],
                question_id=data['question_id'],
                answer_id=data['answers']
            )
        return HttpResponseRedirect(
            reverse_lazy('recruiting:recruit-questions', kwargs={'recruit_id': context['recruit'].id})
        )
