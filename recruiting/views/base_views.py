from django.views.generic.base import TemplateResponseMixin, View
from django.views.generic.edit import ModelFormMixin, FormMixin
from ..models import Sith, Recruit



class BaseView(TemplateResponseMixin, View):
    """Base View, handle sith_id and recruit_id in urls"""
    def get(self, request, *args, **kwargs):
        context = self.get_extra_context(kwargs)
        if hasattr(self, 'get_context_data'):
            context.update(self.get_context_data(context, **kwargs))
        return self.render_to_response(context)

    def get_extra_context(self, kwargs):
        context = {}
        if 'sith_id' in kwargs:
            context['sith'] = self._get_user(Sith, 'sith_id', kwargs)
        if 'recruit_id' in kwargs:
            context['recruit'] = self._get_user(Recruit, 'recruit_id', kwargs)
        return context

    def _get_user(self, model, arg_name, view_kwargs):
        user = None
        try:
            user = model.objects.get(id=int(view_kwargs[arg_name]))
        except model.DoesNotExist:
            print('{0} with id: {1} does not exists'.format(model, view_kwargs[arg_name]))
        return user


class MyListView(BaseView):
    """Base List View for all our cases"""
    def get_context_data(self, context, **kwargs):
        object_name = self.context_object_name \
            if hasattr(self, 'context_object_name') and self.context_object_name \
            else 'object'
        if hasattr(self, 'get_queryset'):
            queryset = self.get_queryset(context, **kwargs)
        elif hasattr(self, 'queryset'):
            queryset = self.queryset
        else:
            raise AttributeError('There is no get_queryset or queryset in class '
                                 + self.__class__.__name__)
        return { object_name: queryset }


class MyDetailView(BaseView):
    """Base Detail View for all our cases"""
    def get_context_data(self, context, **kwargs):
        object_name = self.context_object_name \
            if hasattr(self, 'context_object_name') and self.context_object_name \
            else 'object'
        if not object_name in context:
            if hasattr(self, 'get_object'):
                obj = self.get_object(context, **kwargs)
            elif hasattr(self, 'pk_url_kwarg') and hasattr(self, 'model'):
                if self.pk_url_kwarg in kwargs:
                    obj = self.model.objects.get(pk=kwargs[self.pk_url_kwarg])
                else:
                    raise AttributeError('There is no pk in request')
            else:
                raise AttributeError('There is no get_object or model and pk_url_kwarg in class '
                                     + self.__class__.__name__)
            return { object_name: obj }
        return {}


class MyFormView(BaseView, FormMixin):
    """Base Form View for all our cases"""
    def post(self, request, *args, **kwargs):
        """
        Handle POST requests: instantiate a form instance with the passed
        POST variables and then check if it's valid.
        """
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)



