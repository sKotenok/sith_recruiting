from django.conf import settings
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponseRedirect
from django.db.models.query import QuerySet
from django.core.mail import send_mail
from django.contrib import messages

from .base_views import MyListView, MyDetailView
from ..models import Sith, Recruit


class SithEntry(MyListView):
    queryset = Sith.objects.order_by('name').select_related('planet')
    context_object_name = 'siths'
    template_name = 'recruting/sith/entry.html'


class SithPersonal(MyListView):
    context_object_name = 'recruits'
    template_name = 'recruting/sith/personal.html'

    def get_queryset(self, context, **kwargs):
        if 'sith' in context and context['sith']:
            planet_id = context['sith'].planet_id
            return Recruit.objects.with_exams_finished(planet_id)
        else:
            return QuerySet()

    def get_context_data(self, context, **kwargs):
        new_context = super(SithPersonal, self).get_context_data(context, **kwargs)
        if 'sith' in context and context['sith']:
            new_context['students'] = Recruit.objects.get_sith_students(context['sith'].id)
        return new_context


class SithRecruit(MyDetailView):
    context_object_name = 'recruit'
    template_name = 'recruting/sith/recruit.html'

    def post(self, request, *args, **kwargs):
        context = self.get_extra_context(kwargs)
        sith = context.get('sith', None)
        recruit = context.get('recruit', None)
        if sith and recruit:
            if sith.use_shadow_hand(recruit):
                send_mail(
                    _('Notification from sith recruiting system'),
                    _('Master choose you as his student, wait for an orders'),
                    settings.EMAIL_FROM,
                    [recruit.email],
                    fail_silently=True,
                )
                messages.success(request, 'You get a new student: {0}'.format(recruit.name))
            else:
                messages.error(request, 'You don\'t have any more shadow hands.')
            return HttpResponseRedirect(
                reverse_lazy('recruiting:sith-personal', kwargs={'sith_id': sith.id})
            )
        else:
            return HttpResponseRedirect(reverse_lazy('recruiting:index'))


class SithStudent(MyDetailView):
    context_object_name = 'recruit'
    template_name = 'recruting/sith/student.html'

    def post(self, request, *args, **kwargs):
        context = self.get_extra_context(kwargs)
        sith = context.get('sith', None)
        recruit = context.get('recruit', None)
        if sith and recruit:
            recruit.extermination_status = Recruit.EXTERMINATION_NEED_TO
            recruit.save()
            messages.success(request, 'Student {0} will be exterminated'.format(recruit.name))
            return HttpResponseRedirect(
                reverse_lazy('recruiting:sith-personal', kwargs={'sith_id': sith.id})
            )
        else:
            return HttpResponseRedirect(reverse_lazy('recruiting:index'))


class SithsWithMoreThanOneSH(MyListView):
    queryset = Sith.objects.order_by('name').filter(shadow_hands__gt=1).select_related('planet')
    context_object_name = 'siths'
    template_name = 'recruting/sith/entry.html'
