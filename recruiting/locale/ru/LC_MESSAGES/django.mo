��    7      �  I   �      �  	   �     �     �     �     �     �           #     D     M     \     o          �  =   �  4   �     	       (   (     Q     X     d     l     u     �  	   �     �     �     �     �     �     �     	          $     4     P     Y     o     �     �  	   �     �  %   �     �  '   �       "   "  h   E     �     �     �     �     �  �  �     �
     �
  "   �
       
     +   !     M  =   h     �     �     �     �       
     c      i   �     �  -   �  R   %     x     �     �     �     �  
   �     �     �     �       !   +  !   M     o     ~     �     �     �     �  2   �  =   �     3     F     O  
   V  p   a     �  P   �  4   @  0   u  �   �     E     \  2   s     �     �        (   2      4         +            *              6          1          /       '            %       	               )   $   "      &         .              
   7      0                               ,   #                       !             5   3       -    A recruit Answer Answer this question: Answer:position Answer:text Choose yourself from the list: Choose youself: Complete this registration form: Continue Do not need to Enter as a recruit Enter as a sith Exam answers: Finished If you are already registered, choose yourself from the list. Master choose you as his student, wait for an orders Need to New recruits in planet Notification from sith recruiting system Planet Planet:name Planets Question Question:ordern_uid Question:text Questions Recruit Recruit Answer Recruit Answers Recruit:Extermination status Recruit:Time registered Recruit:age Recruit:email Recruit:name Recruit:teacher RecruitAnswer:Time answered Recruits Register as a recruit Shadow hands count Shadow hands: Sith Sith:name Siths Siths with more than one shadow hands Take as a student The count of questions, You answered is To start answering questions You answered for all the questions You answered to all questions, your master gives you.
				Now you just need to wait, when he choose you. You enter as You entered as Your personal page Your students from planet Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Рекрут Ответить Ответьте на вопрос Позициця Текст Выберите себя из списка Выберете себя: Заполните регистрационную форму: Продолжить Нет нужды Войти как рекрут Войти как ситх Ответы: Готов Если вы уже зарегестрированы, выберите себя из списка. Мастер выбрал вас учеником, ожидайте дальнейших приказов Пора Новые рекруты на планете Уведомление от рекрутинговой системы ситхов Планета Название Планеты Вопрос UUID ордена Текст Вопросы Рекрут Ответ рекрутёра Ответы рекрутёра Статус ликвидации Время регистрации Возраст e-mail Имя Учитель Время ответа Рекруты Зарегестрирован как рекрут Количество испытаний тёмной руки Руки тени: Ситх Имя Ситхи Ситхи, которым позволено более одного испытания теневой руки Взять в ученики Количество вопросов, на которые вы ответили Перейти к ответам на вопросы Вы ответили на все вопросы Вы ответили на все вопросы, заданные мастером.
				Теперь ждите, пока мастер выберет вас. Вы вошли как Вы вошли как Ваша персональная страница Ваши студенты с планеты 