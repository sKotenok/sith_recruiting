from django import forms
from django.contrib import admin
from .models import Planet, Sith, Recruit, Question, Answer


class SithAdmin(admin.ModelAdmin):
    # Options for add/edit page
    fields = ('id', 'name', 'planet', 'shadow_hands')
    readonly_fields = ('id',)
    save_as = True
    save_on_top = True

    # Options for list page
    list_display = ('id', 'name', 'planet')
    list_display_links = ('id', 'name')
    list_per_page = 30
    search_fields = ('name',)
    sortable_by = ('id', 'name', 'planet')
    ordering = ('name',)
    view_on_site = False


class RecruitAdmin(admin.ModelAdmin):
    # Options for add/edit page
    fields = ('id', 'name', 'email', 'age', 'planet', 'teacher')
    readonly_fields = ('id', 'time_registered')
    save_as = True
    save_on_top = True

    # Options for list page
    list_display = ('id', 'name', 'planet', 'time_registered', 'teacher')
    list_display_links = ('id', 'name')
    list_per_page = 30
    search_fields = ('name', 'email')
    ordering = ('name',)
    view_on_site = False


class AnswerInlineForm(forms.ModelForm):
    class Meta:
        widgets = {'text': forms.Textarea(attrs={'cols': 50, 'rows': 2,})}

class AnswerInline(admin.TabularInline):
    model = Answer
    form = AnswerInlineForm
    extra = 3
    fields = ('id', 'text', 'position')
    ordering = ('position',)


class QuestionAdmin(admin.ModelAdmin):
    fields = ('orden_uuid', 'text')
    inlines = (AnswerInline,)


admin.site.register(Planet)
admin.site.register(Sith, SithAdmin)
admin.site.register(Recruit)
admin.site.register(Question, QuestionAdmin)
