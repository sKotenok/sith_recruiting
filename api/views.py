from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from .permissions import IsSuperUserOrReadOnly

from recruiting.models import Question, Planet, Recruit, Sith
from .serializers import PlanetSerializer, RecruitSerializer, SithSerializer, QuestionSerializer


class PlanetViewSet(viewsets.ModelViewSet):
    """API endpoint to view all planets."""
    queryset = Planet.objects.all().order_by('name')
    serializer_class = PlanetSerializer
    permission_classes = (IsSuperUserOrReadOnly,)


class RecruitViewSet(viewsets.ModelViewSet):
    """API endpoint that allows recruits to be viewed, added or edited."""
    queryset = Recruit.objects.all().order_by('name')
    serializer_class = RecruitSerializer
    permission_classes = (AllowAny,)


class SithViewSet(viewsets.ModelViewSet):
    """API endpoint that allows siths to be viewed."""
    queryset = Sith.objects.all().order_by('name')
    serializer_class = SithSerializer
    permission_classes = (IsSuperUserOrReadOnly,)


class QuestionViewSet(viewsets.ModelViewSet):
    """API endpoint that allows siths to be viewed."""
    queryset = Question.objects.all().order_by('-id')
    serializer_class = QuestionSerializer
    permission_classes = (IsSuperUserOrReadOnly,)
