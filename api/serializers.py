from rest_framework import serializers

from recruiting.models import Answer, Question, Planet, Recruit, RecruitAnswer, Sith


class PlanetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Planet
        fields = '__all__'


class SithSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sith
        fields = '__all__'


class RecruitAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecruitAnswer
        fields = '__all__'
    

class RecruitSerializer(serializers.ModelSerializer):
    answers = RecruitAnswerSerializer(many=True)
    class Meta:
        model = Recruit
        fields = '__all__'


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = '__all__'


class QuestionSerializer(serializers.ModelSerializer):
    answers = AnswerSerializer(many=True)
    class Meta:
        model = Question
        fields = '__all__'

