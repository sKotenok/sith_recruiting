from django.urls import path, include
from rest_framework import routers

from .apps import ApiConfig
from . import views



router = routers.DefaultRouter()
router.register('sith', views.SithViewSet)
router.register('recruit', views.RecruitViewSet)
router.register('planet', views.PlanetViewSet)
router.register('question', views.QuestionViewSet)


app_name = ApiConfig.name
urlpatterns = [
    path('', include(router.urls)),
]
